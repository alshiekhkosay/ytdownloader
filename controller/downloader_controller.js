const ytdl = require("@distube/ytdl-core");
const express = require("express");
const fs = require("fs");
const path = require("path");

var currentDownloadTasks = new Set();
/**
 * @param {express.Request} req
 * @param {express.Response} res
 */
exports.downloadVideo = async (req, res, next) => {
  const videoId = req.body.videoId;

  if (!videoId) {
    return res
      .status(400)
      .send({ success: false, message: "videoId params is not provided" });
  }

  try {
    const info = await ytdl.getInfo(videoId);

    if (currentDownloadTasks.has(videoId)) {
      await waitUntilIdLeaves(videoId);
    }

    // Replace any remaining problematic characters
    const sanitizedTitle = info.videoDetails.title.replace(
      /[^a-zA-Z0-9-_]/g,
      ""
    );
    const outputFilePath = `public/${sanitizedTitle}.mp3`;

    return fs.stat(outputFilePath, async function (err, stat) {
      if (err == null) {
        //file exists
        return res.status(200).send({
          url: outputFilePath,
        });
      } else if (err.code === "ENOENT") {
        // file does not exist

        currentDownloadTasks.add(videoId);

        const format = ytdl.chooseFormat(info.formats, {
          filter: (format) =>
            (format.codecs === "opus" ||
              (format.hasAudio === true && format.hasVideo === false)) &&
            format.audioQuality === "AUDIO_QUALITY_LOW",
        });

        const outputStream = fs.createWriteStream(outputFilePath);

        ytdl.downloadFromInfo(info, { format }).pipe(outputStream);

        outputStream.on("finish", () => {
          console.log(`Finished downloading: ${outputFilePath}`);
          res.status(200).send({
            url: outputFilePath,
          });
          currentDownloadTasks.delete(videoId);
          // setTimeout(() => {
          //       fs.unlink(outputFilePath, (err) => {
          //           if (err) {
          //               console.error(`Failed to delete file: ${outputFilePath}`, err);
          //           } else {
          //               console.log(`File deleted: ${outputFilePath}`);
          //           }
          //       });
          //   }, 300000);
        });

        outputStream.on("error", (err) => {
          console.error("Stream error:", err);
          currentDownloadTasks.delete(videoId);

          // fs.unlink(outputFilePath, (err) => {
          //   if (err) throw err;
          //   console.log("file was deleted");
          // });
          res.status(500).send("Error downloading file");
        });
      }
    });
  } catch (err) {
    console.error("Error :", err);
    res.status(500).send("Error getting video info");
    currentDownloadTasks.delete(videoId);
  }
};

function waitUntilIdLeaves(id, interval = 500) {
  return new Promise((resolve) => {
    const intervalId = setInterval(() => {
      if (!currentDownloadTasks.has(id)) {
        clearInterval(intervalId);
        resolve();
      }
    }, interval);
  });
}

/**
 * @param {express.Request} req
 * @param {express.Response} res
 */
exports.deleteFiles = (req, res, next) => {
  dirPath = "public/";
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      console.error("Error reading directory:", err);
      return res.status(500).send({ message: "Error reading directory" });
    }

    let totalFiles = files.length;
    if (totalFiles === 0) {
      return res
        .status(200)
        .send({ message: "No files to delete, directory is empty" });
    }

    files.forEach((file) => {
      const filePath = path.join(dirPath, file);
      fs.stat(filePath, (err, stat) => {
        if (err) {
          console.error("Error getting file stats:", err);
          return res.status(500).send({ message: "Error getting file stats" });
        }

        if (stat.isFile()) {
          fs.unlink(filePath, (err) => {
            if (err) {
              console.error("Error deleting file:", err);
            } else {
              console.log(`Deleted file: ${filePath}`);
              totalFiles--;
              if (totalFiles === 0) {
                res.send({ message: "All files deleted successfully" });
              }
            }
          });
        } else {
          totalFiles--;
          if (totalFiles === 0) {
            res.send({ message: "All files deleted successfully" });
          }
        }
      });
    });
  });
};
